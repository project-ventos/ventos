
test: testpy testc

testpy:
	pytest

testc:
	PATH=${PATH} pio test -e native -d /app/firmware
lint:
	pylint datadictionary/*.py

# Watch processes that use script taking
# 1 - path to watch
# 2 - command to run
dev-watch-test:
	devbin/watch.sh . "make test"
dev-watch-testc:
	devbin/watch.sh . "make testc"

dev-watch-lint:
	devbin/watch.sh . "make lint"

dev-watch-yaml:
	devbin/watch.sh datadictionary "python3.7 datadictionary/package.py --job all -vv --outdir ../ventosdoc"

# outputs the help for the CLI tool `package.py`
yaml-package-help:
	python3 datadictionary/package.py --help

# Processes the source YAML into both C and markdown formats
# This will direct both outputs to the same folder which is probably undesirable
yaml-package:
	python3 datadictionary/package.py --sourcedir datadictionary --job all --outdir autogen

# Package up the YAML into C files
yaml-package-c:
	python3 datadictionary/package.py --sourcedir datadictionary --job c --outdir firmware/lib/state --outfile state_struct.h -vv

# Package up the YAML into C markdown
yaml-package-md:
	python3 datadictionary/package.py --sourcedir datadictionary --job markdown --outdir autodoc --outfile index.md

# Build PlatformIO in a Docker container then load Bash.
# Todo: Clarify why this asks for permission to my documents.
docker-bash:
	docker build . -t ventos:0.1
	docker run -it -v `pwd`:/app ventos:0.1 bash
	# watchmedo shell-command --pattern="/app/datadictionary/*.py" --command="make test" --debug-force-polling

# Build and run PlatformIO native tests in a Docker container.
docker-test-native:
	docker build --tag ventos .
	docker run --rm -it -v `pwd`:/app --name vos ventos bash -c 'cd ventos/firmware &&  platformio lib install && platformio lib list && pio test -e native -v && .pio/build/native/program'
#	docker run --rm -it --name vos ventos bash -c 'cd firmware && rm -rf .pio && platformio lib install && platformio lib list && pio test -e native'

# Build and run PlatformIO uno tests in a Docker container.
docker-test-uno:
	docker build --tag ventos .
	docker run --rm -it --name vos ventos bash -c 'cd firmware && platformio lib install && platformio lib list && pio test -e uno'

docker-run-native:
	docker build --tag ventos .
	docker run --rm -it --name vos ventos bash -c 'cd firmware && rm -rf .pio && platformio lib install && pio run -e native && .pio/build/native/program'


# Build and run src/main.cpp on Mac/Linux
pio-run-native:
	cd firmware \
	&& pio run -e native -v \
	&& .pio/build/native/program

# Build and run src/main.cpp on Windows Powershell
pio-run-native-win:
	cd firmware \
	&& pio run -e native -v \
	&& cd .pio/build/native \
	&& del log.txt \
	&& .\program.exe

# Build and run on FeatherESP32 and start serial monitor
pio-run-esp32:
	cd firmware \
	&& pio run -e featheresp32 -t upload \
	&& pio device monitor -b 19200

# Build and run on Uno and start serial monitor
pio-run-uno:
	cd firmware \
	&& pio run -e uno -t upload \
	&& pio device monitor -b 19200

# Run on the "proportional_valve" hardware
pio-run-polyvent-pv:
	cd firmware \
	&& pio run -e polyvent_pv -t upload \
	&& pio device monitor -b 500000
#	&& pio device monitor -b 9600


# Clean PlatformIO cache
pio-clean:
	rm -rf firmware/.pio

# Apply clang-format to entire repo to pass our format tests
format:
	find . -not -path "./firmware/.pio/*" -iname "*\.h" -o -iname "*\.cpp" | xargs clang-format -i
