#include <debug.h>
#include <localization.h>

namespace VentOS {
bool Localization::setLanguage() { return true; }

bool Localization::setTimezone() { return true; }

bool Localization::setUnits() { return true; }

} // namespace VentOS
