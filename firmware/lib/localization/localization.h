#ifndef LOCALIZATION_H
#define LOCALIZATION_H

namespace VentOS {
enum Language {
  ENGLISH,
  SPANISH,
  CHINESE,
  FRENCH,
  GERMAN
  // etc...
};

enum Timezone {
  UTC,
  PST,
  CST,
  EST,
  SYT,
  NZT
  // etc...
};

enum Units { // height, weight
  METRIC,    // cm, kg
  IMPERIAL   // inches, pounds
};

struct Locale {
  Language language = ENGLISH;
  Timezone timezone = UTC;
  Units units = METRIC;
};

class Localization {
public:
  Localization() {
    //
  }
  bool setLanguage();
  bool setTimezone();
  bool setUnits();
};

} // namespace VentOS
#endif