#include "controller_task_PCV.h"
#include <controller.h>
#include <debug.h>
#include <drive.h>
#include <inttypes.h>

// uint32_t ms_in_inspiration(const universal_mode *mode) {
//   float breath_length = 60.0 / mode->rr_bpm;
//   return (uint32_t) 1000.0 * breath_length * (1.0/(((float)
//   mode->e_to_i/10.0) + 1.0));
// }

// no longer sure what this should return...
void run_experimental_pcv(VentStateT *state, VentSettingsT *settings, Drive *d,
                          uint32_t t) {
  // simply toggle...
  // A major goal now is to to have this use the drive.
  // In presure controlled ventilation, we have a "MAXIMUM_VOL", not a target
  // VOL.

  const uint32_t insp_dur_ms = ms_in_inspiration_controller(
      (uint32_t)settings->targetRR, (uint32_t)settings->targetEI);

  const uint32_t MAX_PRESSURE = settings->maxPressure;

  const uint32_t MAXIMUM_VOL = settings->maxVolume;

  const uint32_t TARGET_PRESSURE = settings->targetPressure;
  //  const uint32_t DRIVE_INTERVAL_MS = 10;
  const float MAXIMUM_FLOW_MLPS =
      (float)MAXIMUM_VOL / ((float)insp_dur_ms / 1000.0);

  // Note: Some settings are expected to change rather rapidly,
  // and these are parameters to the functions below.
  // Others which change more slowly and do not effect the current breath
  // are better thought of as changes to the drive itself. An example of this
  // is FiO2. It is a relatively infrequently changed paramater. It therefore
  // makes the most sense to set it on the driver directly.
  d->setFiO2(settings->targetFiO2);

  if (state->ventFlow == INSPIRING) {
    // We are still in inspriation, so call drive_pressure
    uint32_t d_res =
        d->drive_pressure((uint32_t)MAXIMUM_FLOW_MLPS, TARGET_PRESSURE,
                          MAX_PRESSURE, (char *)d->client_data, "spud");
  } else {
    uint32_t d_res = d->pause_and_prep(10, d->client_data, "pause_and_prep");
  }
}

void no_op_for_testing_pcv(VentStateT *state, VentSettingsT *settings, Drive *d,
                           uint32_t t) {}

void run_experimental_vcv(VentStateT *state, VentSettingsT *settings, Drive *d,
                          uint32_t t) {
  // simply toggle...
  // A major goal now is to to have this use the drive.

  const uint32_t insp_dur_ms = ms_in_inspiration_controller(
      (uint32_t)settings->targetRR, (uint32_t)settings->targetEI);

  const uint32_t MAX_PRESSURE = settings->maxPressure;

  const uint32_t MAXIMUM_VOL = settings->maxVolume;

  const uint32_t TARGET_TIDAL_VOL = settings->targetTidalVolume;

  const uint32_t TARGET_PRESSURE = settings->targetPressure;

  const float TARGET_FLOW_MLPS =
      (float)TARGET_TIDAL_VOL / ((float)insp_dur_ms / 1000.0);

  if (state->ventFlow == INSPIRING) {
    uint32_t d_res =
        d->drive_flow((uint32_t)TARGET_FLOW_MLPS, TARGET_PRESSURE, MAX_PRESSURE,
                      (char *)d->client_data, "spud");
  } else {
    uint32_t d_res = d->pause_and_prep(10, d->client_data, "pause_and_prep");
  }
}

task CONTROLLER_TASKS[TASK_CNT];

void controller_task_init() {
  task pcv = {.run = &run_experimental_pcv};
  CONTROLLER_TASKS[PCV_TASK] = pcv;
  task vcv = {.run = &run_experimental_vcv};
  CONTROLLER_TASKS[VCV_TASK] = vcv;
  task noop = {.run = &no_op_for_testing_pcv};
  CONTROLLER_TASKS[NO_OP_FOR_TESTING_TASK] = noop;
}
