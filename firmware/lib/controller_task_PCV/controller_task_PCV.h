#ifndef CONTROLLER_TASK_PCV_H
#define CONTROLLER_TASK_PCV_H 1

#include <controller.h>
#include <drive.h>
#include <inttypes.h>

using namespace VentOS;

// NOTE: This is an example of where the "Data Dictionary" would be useful to us
// Quite possibly that should simply be a .h file (at least, one embodiment of
// it is.)
// TODO: The Ventilator state here should probably be moved out to its own file.
#define PCV_MODE 0
#define VCV_MODE 1

struct universal_mode {
  uint8_t mode;
  float rr_bpm;
  float e_to_i;
  float max_pressure_cmH2O;
};

struct pcv_mode {
  float target_pressure_cmH2O;
  float max_volume_ml;
};

struct vcv_mode {
  float target_volume_ml;
  float max_pressure_cmH2O;
};

union ModeSpecifics {
  pcv_mode pcv;
  vcv_mode vcv;
};

typedef void (*task_run_ms)(VentOS::VentStateT *state,
                            VentOS::VentSettingsT *settings, Drive *d,
                            uint32_t t);

struct task {
  task_run_ms run;
};

// Run experimental Pressure Controlled Ventilation for t millisedons
// Take a state and return a state
void run_experimental_pcv(VentStateT *state, VentSettingsT *settings, Drive *d,
                          uint32_t t);

void run_experimental_vcv(VentStateT *state, VentSettingsT *settings, Drive *d,
                          uint32_t t);

#define TASK_CNT 3

#define PCV_TASK 0
#define VCV_TASK 1
#define NO_OP_FOR_TESTING_TASK 2

extern task CONTROLLER_TASKS[TASK_CNT];

void controller_task_init();

#endif
