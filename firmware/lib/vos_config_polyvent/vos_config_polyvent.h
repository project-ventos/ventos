/*
This file is HIGHLY POLYVENT specific;
it will capture in particular sensors offered by the
PolyVent machine

 */

#ifndef VOS_CONFIG_POLYVENT_H
#define VOS_CONFIG_POLYVENT_H
#include <sensor.h>
// #include <vos_simulator.h>
#include <controller.h>
#include <polyvent_drive_pv.h>

// This will likely be redefinition, but that is okay...
#define SENSOR_CNT 0

extern sensor SENSORS[];

void set_ventilator_state(VentStateT *v, VentSettingsT *vset);

#endif
