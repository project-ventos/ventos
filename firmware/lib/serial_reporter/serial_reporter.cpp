#include <PIRDS.h>
#include <debug.h>
#include <sensor.h>
#include <serial_reporter.h>

using namespace VentOS;

extern sensor SENSORS[];
extern uint8_t sensor_cnt;

#define DEBUG_SR 0

bool SerialReporter::setup() {
  DebugLnCC("Serial Reporter setup");

  return true;
}

Measurement get_measurement(char event, char mtype, char loc, uint8_t num,
                            uint32_t ms, int32_t value) {
  Measurement ma;
  ma.event = event;
  ma.type = mtype;
  ma.loc = loc;
  ma.num = 0 + num;
  ma.ms = ms;
  ma.val = value;
  return ma;
}
void output_on_serial_print_PIRDS(char e, char t, char loc,
                                  unsigned short int n, unsigned long ms,
                                  signed long val) {
  Measurement ma = get_measurement(e, t, loc, n, ms, val);
  // I need a proof that this buffer is larger enough, but I think it is...
  char buff[256];
  int rv = fill_JSON_buffer_measurement(&ma, buff, 256);
  if (rv <= 0) {
    DebugLnCC("Internal Error in PIRDS library");
    DebugLn(rv);
  }
// TODO: If ARDUINO is not defined, you probably don't have a serial port at
// all!!
#ifdef ARDUINO
  Serial.print(buff);
#endif
}

bool SerialReporter::run(uint32_t msNow) {
#if DEBUG_SR > 0
  DebugLnCC("Serial Reporter run");
  DebugLn(msNow);

  // First, we want to know what mode we are in from the state...
  // This is oddly named "VentFlow on the state...
  switch (vstate->ventFlow) {
  case INSPIRING:
    DebugLnCC("INSPIRING");
    break;
  case EXHALING:
    DebugLnCC("EXHALING");
    break;
  case OFF:
    DebugLnCC("OFF");
    break;
  case ERROR_NO_FLOW:
    DebugLnCC("ERROR_NO_FLOW");
    break;
  case ERROR_OTHER:
    DebugLnCC("ERROR_OTHER");
    break;
  default:
    DebugLnCC("INTERNAL ERROR! BAD FLOW");
  }
#endif

#if !(defined(INCLUDE_POLYVENT_CONFIG) || defined(INCLUDE_POLYVENT_CONFIG_PV))
  // Now just testing ability to output a pressure...
  sensor_func s = find_sensor(SENSORS, 'D', 'I', 0);
  if (s) {
    uint32_t v = s();
    //    DebugLn(v);
    swTimer.update();
    uint32_t ms = swTimer.elapsed();
    output_on_serial_print_PIRDS('M', 'D', 'I', 0, ms, v);
    // DebugLnCC("");

  } else {
    DebugLnCC("NO DIFFERENTIAL SENSOR OR MOCK SENSOR!");
  }

  // Now I will also attempt to construct a mock flow sensor...
  sensor_func f = find_sensor(SENSORS, 'F', 'I', 0);
  if (f) {
    uint32_t v = f();
    //    DebugLn(v);
    swTimer.update();
    uint32_t ms = swTimer.elapsed();
    output_on_serial_print_PIRDS('M', 'F', 'I', 0, ms, v);
    // DebugLnCC("");
  } else {
    DebugLnCC("NO SENSOR OR MOCK SENSOR!");
  }
#endif

  return true;
}

void SerialReporter::setVentSettingsPointer(VentSettings *vsettings) {
  this->vsettings = vsettings;
}
void SerialReporter::setVentStatePointer(VentState *vstate) {
  this->vstate = vstate;
}
