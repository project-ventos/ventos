#ifndef PATIENT_H
#define PATIENT_H

#include <inttypes.h>
namespace VentOS {
enum Gender { MALE, FEMALE, NOT_SET };

struct PatientData {
  Gender gender;
  uint16_t weight;
  uint16_t height;
  uint16_t idealBodyWeight;
};

// bool validatePatientData(PatientData pd)
// bool setPatientData(PatientData pd)
// PatientData getPatientData()
} // namespace VentOS

#endif