#ifndef TIMER_H
#define TIMER_H

#ifndef ARDUINO
#include <chrono>
using namespace std::chrono;
#endif

#include <inttypes.h>

namespace VentOS {

#ifndef ARDUINO
uint64_t timeSinceEpochMs();
#endif

// Software timer that can be used on Arduino or native
class Timer {
private:
  uint32_t msElapsed;
  uint32_t msStart;
  uint32_t msLast;

public:
  Timer();
  Timer(uint32_t start) {
    msElapsed = 0;
    msStart = start;
    msLast = 0;
  }
  bool update();
  uint32_t elapsed();
  uint32_t getLast();
  uint32_t setLast(uint32_t ms);
  uint32_t diff();
};
} // namespace VentOS

#endif
