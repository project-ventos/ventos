#ifndef SENSOR_HONEYWELL_H
#define SENSOR_HONEYWELL_H

#include <sensor.h>

/* typedef unsigned long (*sensor_func)(); */

/* struct sensor { */
/*     char type; */
/*     char loc; */
/*     uint8_t  num; */
/*     sensor_func func; */
/* }; */

/* HSC *************************************************/
// This is for the HSCDRRT001PD2A3, if you change the Honeywell differential
// pressure sensor, you will have to change this. Thanks to:
// https://github.com/rodan/honeywell_hsc_ssc_i2c for inspiration.
#define HSC_ADDR 0x28
const long unsigned MAX_RANGE = ((1 << 14) - 1);
const float SENSOR_MIN = MAX_RANGE * 0.1;
const float SENSOR_MAX = MAX_RANGE * 0.9;
const uint16_t OUTPUT_MIN_U16 = ((uint16_t)SENSOR_MIN);
const uint16_t OUTPUT_MAX_U16 = ((uint16_t)SENSOR_MAX);
#define PRESSURE_MIN -6894.76 // -1psi  in pa
#define PRESSURE_MAX 6894.76  // 1psi in pa

struct hsc_data {
  uint8_t status;            // 2 bits
  uint16_t pressure_data;    // 14 bits
  uint16_t temperature_data; // 11 bits
};

uint8_t load_hsc_data(const uint8_t addr, struct hsc_data *data);
float get_HSC_pressure(const struct hsc_data raw, const uint16_t output_min,
                       const uint16_t output_max, const float pressure_min,
                       const float pressure_max);
float get_HSC_temperature(const struct hsc_data raw);
uint32_t readHSCPressure();
uint32_t differential_sensor();
void local_setup();

#endif
