#include <Arduino.h>
// #include <unity.h>
#include <Wire.h>
#include <debug.h>
#include <sensor_honeywell.h>

uint8_t load_hsc_data(const uint8_t addr, struct hsc_data *data) {
  uint8_t i;
  uint8_t val[4];
  Wire.requestFrom(addr, (uint8_t)4);
  for (i = 0; i <= 3; i++) {
    //     delay(4);
    val[i] = Wire.read();
  }
  data->status = (val[0] & 0xc0) >> 6; // first 2 bits from first byte
  data->pressure_data = ((val[0] & 0x3f) << 8) + val[1];
  data->temperature_data = ((val[2] << 8) + (val[3] & 0xe0)) >> 5;
  if (data->temperature_data == 65535)
    return 4;
  return data->status;
}

float get_HSC_pressure(const struct hsc_data raw, const uint16_t output_min,
                       const uint16_t output_max, const float pressure_min,
                       const float pressure_max) {
  return ((raw.pressure_data - output_min) * (pressure_max - pressure_min) /
          (output_max - output_min)) +
         pressure_min;
}

float get_HSC_temperature(const struct hsc_data raw) {
  return (raw.temperature_data * 0.0977) - 50;
}

uint32_t readHSCPressure() {
  struct hsc_data ps;

  uint8_t status = load_hsc_data(HSC_ADDR, &ps);

  float p;
  if (status == -1) {
    VentOS::Debug<const char *>("err sensor missing");
    return LONG_MIN;
  } else {
    if (status == 3) {
      Serial.print("err diagnostic fault ");
      Serial.println(ps.status, BIN);
    }
    if (status == 2) {
      // if data has already been feched since the last
      // measurement cycle
      VentOS::Debug<const char *>("warn stale data ");
      //       Serial.println(ps.status, BIN);
    }
    if (status == 1) {
      // chip in command mode
      // no clue how to end up here
      VentOS::Debug<const char *>("warn command mode ");
      //       Serial.println(ps.status, BIN);
    }
    p = get_HSC_pressure(ps, OUTPUT_MIN_U16, OUTPUT_MAX_U16, PRESSURE_MIN,
                         PRESSURE_MAX);
    // convert to 10th of a cm H2O
    p = p * 0.0101972 * 10;
  }
  return (uint32_t)p;
}

uint32_t differential_sensor() {
  uint32_t p = (uint32_t)readHSCPressure();
  return p;
}

void local_setup() { Wire.begin(); }
