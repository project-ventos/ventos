/* =====================================================================================
MIT License

Copyright (c) 2020 Public Invention

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

 *
 * Filename:  vos_simulator.cpp
 *
 * Description:
 *
 * Version:  1.0
 * Created:  09/28/2020 14:53:43
 * Revision:  none
 * Compiler:  gcc
 *
 * Author:  Unni Nair unnivsn@gmail.com
 * Organization:  Public Invention
 * License:  MIT
 *
 * =====================================================================================
 */

#include <stdlib.h>

#include <debug.h>
#include <vos_simulator.h>

static PressureSensorConfigT pr_cfg;
static InternalTestConfiguration it_cfg;
static InternalTestConfiguration *p_it_cfg = 0;

void set_pressure_sensor_state(PressureSensorConfigT *snsr_cfg) {
  if (!p_it_cfg)
    p_it_cfg = &it_cfg;
  if (snsr_cfg)
    p_it_cfg->pressure_cfg = snsr_cfg;
  else
    p_it_cfg->pressure_cfg = &pr_cfg;
}

InternalTestConfiguration *getDefaultInternalTestConfiguration() {
  if (!p_it_cfg)
    set_pressure_sensor_state();
  return p_it_cfg;
}

int32_t simulatedMockPressureReading(
    VentState *vent_state,
    VentSettingsT *vset) { // single method to call from ext i/f
  return VosPressureSimulator::getMockReading(vent_state, vset, true);
}

int32_t simulatedMockFlowReading(VentState *vent_state, VentSettingsT *vset) {
  return VosFlowSimulator::getMockReading(vent_state, vset, true);
}

int32_t VosFlowSimulator::getMockReading(VentState *vent_state,
                                         VentSettingsT *vset, bool normal) {
  if (!vent_state || !vent_state->client_data)
    return 0;
  //        if (!cfg)
  //		return 0;
  if (vent_state->ventFlow != INSPIRING) { // in the lower pressure region
    return 0;
  }
  // else send a flow...
  // I'm doing the simplest thing imaginable now,
  // later we must build a better flow.
  // PIRDS unit of low are ml/min.

  // I'm just adding a defualt
  int32_t tidal_volume_ml =
      vset->targetTidalVolume ? vset->targetTidalVolume : 450;
  // To get the flow, we will take tidal_volume_ml
  // and divide by inspiration time, and convert to a flow in minutes.
  float bpm = vset->targetRR / 10.0;
  float minute_volume_ml = tidal_volume_ml * bpm;
  //        float inspiration_fraction = 1.0 / (1.0 + (vset->targetEI/10.0));
  //        uint16_t flow_ml_per_min  = minute_volume_ml / inspiration_fraction;
  int32_t flow_ml_per_min = minute_volume_ml * (1.0 + (vset->targetEI / 10.0));
  return flow_ml_per_min;
}

int32_t simulatedMockAbnormalPressureReading(
    VentState *vent_state,
    VentSettingsT *vset) { // single method to call from ext i/f
  return VosPressureSimulator::getMockReading(vent_state, vset, false);
}

// this became static method, may become C method in future
int32_t VosPressureSimulator::getMockReading(VentState *vent_state,
                                             VentSettingsT *vset, bool normal) {
  if (!vent_state || !vent_state->client_data)
    return 0;
  pressure_sensor_config *cfg =
      ((InternalTestConfiguration *)(vent_state->client_data))->pressure_cfg;
  cfg->high = vset->targetPressure;

  if (!cfg)
    return 0;

  int addtnl = (normal) ? 0 : cfg->high; // if abnormal pressure, add extra
  int noise = rand() % (cfg->delta);
  if (vent_state->ventFlow != INSPIRING) { // in the lower pressure region
    return vset->targetPEEP;
    // return (cfg->low + noise + addtnl);
  }
  // else... send upper pressure
  return (unsigned int)(cfg->high - noise + addtnl);
}

#if 0
// code below is currently unused.
// keep it for few more cycles before removing it. There is so much effort put into it.
// might be useful if design changes.


VosSimulatorConfig glbl_vos_sim_cfg;

VosSimulatorConfig *getDefaultCfg() {
	return &glbl_vos_sim_cfg;
}

VosPressureSimulator glbl_pr_sim(glbl_vos_sim_cfg);

int32_t simulatedPressureReading(bool insp) { // single method to call from ext i/f
	return glbl_pr_sim.getMockReading(insp);
}



int VosPressureSimulator::getReading() {
        this.curr_count++;
	this.curr_count%= (cfg.pr_cfg.period);

	int noise= rand()%(cfg.pr_cfg.delta);
	if (this.curr_count > (cfg.pr_cfg.high_period)) {	// it is in the lower pressure region
		return (cfg.pr_cfg.run_low)+ noise;
	}
	// else send upper pressure
	return (cfg.pr_cfg.run_up)- noise;
}

int32_t VosPressureSimulator::getMockReading(bool inspiring) {
	int noise= rand()%(cfg.pr_cfg.delta);
	if (!inspiring) {	// it is in the lower pressure region
		return (unsigned)0; // (cfg.pr_cfg.run_low + noise);
	}
	// else send upper pressure
	return (unsigned int)(cfg.pr_cfg.run_up - noise);
}

int VosPressureSimulator::increasePressure(int inc) {
	if ((cfg.pr_cfg.run_up + inc) > cfg.pr_cfg.abs_max)
		cfg.pr_cfg.run_up= cfg.pr_cfg.abs_max;
	else
		cfg.pr_cfg.run_up+= inc;
	return cfg.pr_cfg.run_up;
}

int VosPressureSimulator::decreasePressure(int dec) {
	if ((cfg.pr_cfg.run_up - dec) < cfg.pr_cfg.abs_min)
		cfg.pr_cfg.run_up= cfg.pr_cfg.abs_min;
	else
		cfg.pr_cfg.run_up-= dec;
	return cfg.pr_cfg.run_up;
}

VosFlowSimulator glbl_fl_sim(glbl_vos_sim_cfg);

int32_t simulatedFlowReading(bool insp) { // single method to call from ext i/f
	return glbl_fl_sim.getMockReading(insp);
}


int32_t VosFlowSimulator::getReading() {
	this.curr_count++;
        //	this.curr_count%= (cfg.pr_cfg.period);
        //
        //	int noise= rand()%(cfg.pr_cfg.delta);
        //	if (this.curr_count > (cfg.pr_cfg.high_period)) {	// it is in the lower pressure region
        //		return (cfg.pr_cfg.run_low)+ noise;
        //	}
	// else send upper pressure
        //	return (cfg.pr_cfg.run_up)- noise;
}


int32_t VosFlowSimulator::getMockReading(bool inspiring) {
	if (!inspiring) {	// it is in the lower pressure region
		return (unsigned)0; // (cfg.pr_cfg.run_low + noise);
	} else { // This is just a mock value
        return 357;
        }
}

#endif // #if 0
