/* =====================================================================================
MIT License

Copyright (c) 2020 Public Invention

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

 *
 * Filename:  vos_sim_config.h
 *
 * Description:
 *
 * Version:  1.0
 * Created:  09/28/2020 14:53:43
 * Revision:  none
 * Compiler:  gcc
 *
 * Author:  Unni Nair unnivsn@gmail.com
 * Organization:  Public Invention
 * License:  MIT
 *
 * =====================================================================================
 */

#ifndef __VOS_SIM_CONFIG_H__
#define __VOS_SIM_CONFIG_H__

typedef struct pressure_sensor_config {
  int low;
  int high;
  int delta;
  /* pressure_sensor_config() { */
  /* 	low= 0; */
  /* 	high= 200; */
  /* 	delta= 10; */
  /* } */
  /* pressure_sensor_config(int lowlevel, int maxlevel, int del) { */
  /* 	low= lowlevel; */
  /* 	high= maxlevel; */
  /* 	delta= del; */
  /* } */
} PressureSensorConfigT;

typedef struct InternalTestConfigurationStruct {
  PressureSensorConfigT *pressure_cfg;
  /* InternalTestConfigurationStruct() { */
  /* 	pressure_cfg= 0; */
  /* } */
} InternalTestConfiguration;

InternalTestConfiguration *getDefaultInternalTestConfiguration();

int32_t simulatedMockPressureReading(VentStateT *vent_state,
                                     VentSettingsT *vset);
int32_t simulatedMockFlowReading(VentStateT *vent_state, VentSettingsT *vset);

int32_t simulatedMockAbnormalPressureReading(VentStateT *vent_state,
                                             VentSettingsT *vset);

void set_pressure_sensor_state(pressure_sensor_config *snsr_cfg = 0);

#if 0

// keep below for few more cycles of code modification as the original
// intention is not fully understood/explained in the review.
// will be handy if requirment/design changes

struct VosSimPressureConfig {
	const int abs_min= 0;	// absolute minimum pressure, can never be changed programmatically (change the value here if absolutely necessary)
	const int abs_max= 250;	// absolute maximum pressure, can never be changed programmatically
	int run_low;			// minimum expected pressue at run time, can be modified while running (expected to control via feed-back loop) - delta (subtracted)
	int run_up;				// maximum expected pressue during run time, can be modified while running (control via feed-back) + delta added
	int delta;				// delta value, maximum fluctuation allowed below run_up ( and above run_low); configured during system configuration
	int period;				// number of samples per cycle; say 300 samples for 3 sec cycle
	int high_period;		// number of samples per cycle that supply high pressure (rest will be min pressure); say 100 samples out of 300 (value in period)
	VosSimPressureConfig() {
		run_low= 0; run_up= 200; delta= 10;	// hopefully never used (these default values)
		period= 300; high_period= 100;
		check();
	}
	VosSimPressureConfig(int up, int low, int del, int perd= 300, int hperd= 100) {
		run_low= low; run_up= up; delta= del;
		period= perd; high_period= hperd;
		check();
	}
	void operator=(VosSimPressureConfig &cfg) {
		run_low= cfg.run_low; run_up= cfg.run_up; delta= cfg.delta;
		period= cfg.period; high_period= cfg.high_period;
		check();
	}
	void check() {
		if (run_low<abs_min) run_low= abs_min;
		if (run_up>abs_max) run_up= abs_max;
		if (delta>run_up/10) delta= run_up/10;
	}
};

// add additional config structs here, as required. Ex. temperature.

struct VosSimulatorConfig {
	VosSimPressureConfig pr_cfg;
	VosSimulatorConfig() : pr_cfg() {}	// suggest avoid calling default constructor; use other consturctor and supply proper values.
	VosSimulatorConfig(VosSimPressureConfig &prcon) {
		this->pr_cfg= prcon;
	}
};

// not expected to use this, but may be handy
VosSimulatorConfig *getDefaultCfg();
#endif // #if 0

#endif //	__VOS_SIM_CONFIG_H__
