#ifndef INPUT_H
#define INPUT_H

#include <PIRCS.h>
#include <PIRDS.h>
#include <serial_listen.h>
#include <task.h>

namespace VentOS {

// The input implementation should inherit from this class
class InputController : public Task {
public:
  VentilatorUI *ventui;
  InputController() {}
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
  bool setup(VentilatorUI *ventui);
  bool run(uint32_t msNow, SetCommand *sc);
#pragma clang diagnostic pop

  virtual bool connect();
  virtual bool getMessage(Message m);
  virtual bool getMeasurement(Measurement m);
  // ...
};

} // namespace VentOS

#endif
