#ifndef ALARM_HARDWARE_H
#define ALARM_HARDWARE_H

namespace VentOS_Alarm {

void setPins(int led, int buzzer);
void triggerAlarm(bool on);

} // namespace VentOS_Alarm

#endif