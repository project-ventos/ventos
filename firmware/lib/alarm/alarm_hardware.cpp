#include <alarm_hardware.h>
#include <debug.h>

#ifdef ARDUINO
#include <Arduino.h>
#endif

namespace VentOS_Alarm {

void setPins(int led, int buzzer) {
#ifdef ARDUINO
  pinMode(led, OUTPUT);
  pinMode(buzzer, OUTPUT);
#endif
}

void triggerAlarm(bool on) {
  VentOS::Debug("triggerAlarm: ");
  VentOS::DebugLn(on);

  // digitalWrite(led, HIGH);
  // digitalWrite(buzzer, HIGH);
}

} // namespace VentOS_Alarm
