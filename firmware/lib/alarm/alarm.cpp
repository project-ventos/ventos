#include <alarm.h>
#include <alarm_hardware.h>
#include <debug.h>
#include <task.h>
#include <timer.h>
using namespace VentOS;

namespace VentOS_Alarm {

bool AlarmController::addRecord(AlarmRecord ar) {
  if (arIndex >= 0 && arIndex < arLength) {
    alarmRecords[arIndex] = ar;
    arIndex++;
    return true;
  } else if (arIndex >= arLength) {
    arIndex = 0;
    alarmRecords[arIndex] = ar;
    return true;
  } else {
    return false;
  }
}

AlarmRecord AlarmController::getRecord(int i) {
  //  Check limits
  return this->alarmRecords[i];
}

AlarmState AlarmController::setState(AlarmState state) {
  return this->state = state;
}

AlarmState AlarmController::getState() { return this->state; }

AlarmEvent AlarmController::setEvent(AlarmEvent event) {
  return this->event = event;
}

AlarmEvent AlarmController::getEvent() { return this->event; }

bool AlarmController::silence(AlarmEvent event, uint32_t duration) {
  silentDurations[event] = duration;
  return true;
}

// TODO: this should be an interrupt
void AlarmController::bistableAlarmTimer() {
  timer.update();
  uint32_t diff = timer.diff();
  // Debug("Alarm mod: ");
  // DebugLn(timer.elapsed() % alarmPeriod);

  if (timer.elapsed() % alarmPeriod <= 20) {
    if (!alarmLatch) {
      alarmOn = !alarmOn;
      alarmLatch = true;
      triggerAlarm(alarmOn);
    }
  } else {
    alarmLatch = false;
  }
  // if the time difference is non trivial
  if (diff > 0) {
    // tick every silence timer if it's nonzero
    for (int i = 0; i < durLength; i++) {
      if (silentDurations[i] > 0) {
        // ensure no underflow
        if (diff > silentDurations[i]) {
          silentDurations[i] = 0;
        } else {
          silentDurations[i] -= diff;
        }
      }
    }
  }
  timer.setLast(timer.elapsed());
}

bool AlarmController::trigger(AlarmEvent event, uint32_t time,
                              uint32_t duration) {
  // setState(state); change state?
  if (silentDurations[event] <= 0) {
    return true;
  } else {
    return false;
  }
}

////////// EXTERNAL API ///////////

// Call once to setup the alarm module.
bool AlarmModule::setup() {
  if (ac.setState(Ok)) {
    // Handle error
    DebugCC("Error starting alarms!");
    return false;
  }
  //    DebugLn<const char*>("Alarm module setup");
  return true;
}

// Equivalent to Arduino loop function
bool AlarmModule::run(uint32_t msNow) {
  // This was preliminary code; I'm remvoing it for now...
  ac.bistableAlarmTimer();
  return true;
}

// Silence an alarm event.
bool AlarmModule::silence(AlarmEvent event, uint32_t msNow,
                          uint32_t silentDuration) {
  AlarmRecord ar(event, msNow, silentDuration);
  ac.addRecord(ar);
  return ac.silence(event, silentDuration);
}

// Trigger an alarm event.
bool AlarmModule::trigger(AlarmEvent event, uint32_t msNow, uint32_t duration) {
  AlarmRecord ar(event, msNow, duration);
  ac.addRecord(ar);
  return ac.trigger(event, msNow, duration);
}

// Reset all alarms.
bool AlarmModule::reset() { return true; }

} // namespace VentOS_Alarm

/*
AlarmState AlarmAPI::silence(AlarmEvent event, uint32_t ms) {
    // TODO: set a timer for the event that triggered the alarm
    this->event = event;
    return this->state = SILENCED;
  }

  AlarmRecord AlarmAPI::trigger(AlarmEvent event, uint32_t time, uint32_t
duration){ AlarmRecord ar(event, time, duration);
    // Do something...
    return ar;
  }
  */
