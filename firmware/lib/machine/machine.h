#ifndef MACHINE_H
#define MACHINE_H

#include <controller.h>
#include <inttypes.h>
#include <serial_listen.h>
#include <task.h>

namespace VentOS {

enum PartStatus {
  GOOD,           // Within parameters
  WARNING,        // Part needs servicing soon
  ERROR_ACCURACY, // Errors when sensor parameters are out of bounds
  ERROR_PRECISION,
  ERROR_DISCONNECTED,
  ERROR_FAILURE
};

enum MachineResult {
  MachineFailed,
  MachineSuccess,
  MachineError1,
  MachineConfigError,
  MachineSensorConfigError
  // ....
};

enum MachineStage {
  Standby,        // Powered on, but hasn't received commands in x (5?) minutes
  Starting,       // Uninteruptable startup phase
  FactoryTest,    // Long running burn-in test at the factory
  StartupTest,    // Automatic test run during every startup
  ServiceTest,    // Technician triggered diagnostic test
  SelfTest,       // Very short automatic test that runs periodically while the
                  // ventilator is on
  Ready,          // Ready to do something
  Setup,          // Setup the ventilator
  MachineRunning, // Ventilator is ventilating
  // Busy, // Busy doing something else besides a ventilation task
  // PausedM, // Manually paused until manually unpaused
  // Paused3Sec, // Manually paused for 3 seconds (insp hold manouver)
  // Alarm, // Ventilator has stopped because an alarm has been raised
  Disabled,     // eg. emergency stop
  ShuttingDown, // Uninterruptable shutdown sequence
  Restarting    // Uninterruptable restart sequence
};

struct MachineState {
  PartStatus batteryStatus = GOOD;
  PartStatus mainsPowerStatus = GOOD;
};

class MachineController : public Task {
private:
  MachineStage stage;
  MachineState state;

public:
  MachineController() {
    //
  }

  // Note this is an explicit override of the virtual function.
  //#pragma clang diagnostic push
  //#pragma clang diagnostic ignored "-Woverloaded-virtual"
  bool setup(VentilatorUI *ventui);
  //#pragma clang diagnostic pop
  bool run(uint32_t msNow);
  MachineResult bootMachine(); // Very first method to call
  MachineResult setupMachine();
  MachineResult startMachine();
  MachineResult restartMachine();
  MachineResult shutdownMachine();
  VentController vc;
};

} // namespace VentOS

#endif
/*

      // Localization could be in another class?
      bool setLanguage();
      bool setTimezone();
      bool setPatientUnits();

      // Diagnostic testings could be in another class?
      bool runSelfTest();
      bool runStartupTest();
      bool runFactoryTest();
      bool runServiceTest();

      // History could be in another class?
      bool addHistory();
      bool readHistory(int i);
      bool getHistoryBetween(int i, int j);
      bool getAllHistory();
      bool clearHistory();
*/
