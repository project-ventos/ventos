#ifdef ARDUINO
#include <Arduino.h>
#else
#include <iostream>
#endif

#include <debug.h>
#include <serial_listen.h>
#include <stdio.h>
#include <string.h>

using namespace VentOS;

#define DEBUG_SERIAL_LISTEN 3
bool UI::initialization() {

  initialization_success = false;
  input_buffer[0] = '\0';
  return initialization_success = true;
} // Setup communication channel

// The clears out the current Serial buffer, and
// also sets the current input_buffer to null.
// This is appropriate to call when we know we
// have processed a one-character command, which
// include the emergency stop commands.
int UI::clear_buffers(char buffer[]) {
#ifdef ARDUINO
  Serial.readBytesUntil('\n', buffer, INPUT_BUFFER_SIZE - 1);
#endif
  input_buffer[0] = '\0';
}

bool UI::one_char_command_found(int num_read, char buffer[], int k) {

  // HACK ALERT!! For debugging, we want VERY FAST commands to disable and
  // and home the ventilator. Because this is actully being
  // done by a human being, we want this to be a single, one-character command:
  // s - enter emergency stop; enter emergency stop mode.
  // c - exit emergency stop mode; continue operations.
  // 1 - perform precisely 1 breath, then enter emergency stop mode
  // 9 - perform precisely 9 breaths, then enter emergency stop mode
  // h - home the machine, even if in emergency stop mode.
  // Because this routine is designed to return a buffer that
  // contains a PIRCS command, we expand the PIRCS commands,
  // and return the command in the buffer, where it will be read
  // by the PIRCS library. Then how we interpret it later happens
  // in a different part of the code.
  char c = 0;
  if ((num_read == 2 &&
       (input_buffer[k - 1] == 's' || input_buffer[k - 1] == 'c' ||
        input_buffer[k - 1] == '1' || input_buffer[k - 1] == 'h'))) {
    c = input_buffer[k - 1];
  }
  if ((num_read == 1 && (input_buffer[k] == 's' || input_buffer[k] == 'c' ||
                         input_buffer[k] == '1' || input_buffer[k] == 'h'))) {
    c = input_buffer[k];
  }
#if DEBUG_SERIAL_LISTEN > 0
  DebugLnCC("testing one character command");
#endif

  switch (c) {
  case 's':
    strcpy(
        buffer,
        "{\"com\":\"C\",\"par\":\"M\",\"int\":\"s\",\"mod\":\"U\",\"val\":0}");
    //      clear_buffers(input_buffer);
    return true;
    break;
  case 'c':
    strcpy(
        buffer,
        "{\"com\":\"C\",\"par\":\"M\",\"int\":\"c\",\"mod\":\"U\",\"val\":0}");
    //      clear_buffers(input_buffer);
    return true;
    break;
  case '1':
    strcpy(
        buffer,
        "{\"com\":\"C\",\"par\":\"M\",\"int\":\"1\",\"mod\":\"U\",\"val\":0}");
    //      clear_buffers(input_buffer);
    return true;
    break;
  case 'h':
    strcpy(
        buffer,
        "{\"com\":\"C\",\"par\":\"M\",\"int\":\"h\",\"mod\":\"U\",\"val\":0}");
    //      clear_buffers(input_buffer);
    return true;
    break;
  }
  return false;
}

bool UI::listen(char buffer[], int length) {

  // If we aren't inside the Arduino environment,
  // we probably can't do a serial listen at all, so this
  // is for compiling..
#ifndef ARDUINO
  return false;
#else

  new_from_UI = false;

#if DEBUG_SERIAL_LISTEN > 3
  DebugLnCC("Start");
  DebugLn<const int>(Serial.available());
#endif
  if (Serial.available()) {
    new_from_UI = true;
    int n = strlen(input_buffer);

    // NOTE: This code might not handle two commands that come in
    // at one time!
    // We insist that every command begin with a curly brace '{'...
    // So we delete all characters up to the first curly brace...
    char *position_ptr = strchr(input_buffer, '{');
    if (!position_ptr) {
      position_ptr = input_buffer + n;
    }
    int to_del = position_ptr - input_buffer;

    strncpy(input_buffer, position_ptr, n - to_del);
    input_buffer[n - to_del] = '\0';
    n = n - to_del;

#if DEBUG_SERIAL_LISTEN > 1
    DebugCC("Starting with:");
    DebugLn<const int>(n);
#endif
    // We have to be very careful with our sizes here...
    size_t num_read = Serial.readBytesUntil('\n', input_buffer + n, 255 - n);

#if DEBUG_SERIAL_LISTEN > 1
    DebugLnCC("num_read");
    DebugLn<int>(num_read);
#endif

    // Here we need to check that n+num_read+1 is less that the size of the
    // input_buffer...
    input_buffer[n + num_read] = '\0'; // a zero byte, ASCII NUL
    int k = n + num_read - 1;
#if DEBUG_SERIAL_LISTEN > 0
    DebugLn<char>(input_buffer[k]);
    DebugLn<char>(input_buffer[k - 1]);
#endif

    if (one_char_command_found(num_read, buffer, k)) {
      return true;
    }

    while (isspace(input_buffer[k])) {

#if DEBUG_SERIAL_LISTEN > 0
      DebugLnCC("REMOVING CHARACTER");
#endif
      input_buffer[k] = '\0';
      k--;
    }
    // now see if a closing brace exists in the buffer...
    n = strlen(input_buffer);

#if DEBUG_SERIAL_LISTEN > 0
    DebugLnCC("length after read: ");
    DebugLn<int>(n);
    // now what we really want to do is look for a closing curly brace...

    DebugLnCC("input buffer");
    Debug<const char *>(input_buffer);
    DebugLnCC("|");
#endif

    if (input_buffer[n - 1] == '}') {
#if DEBUG_SERIAL_LISTEN > 0
      DebugLnCC("Found End of Line!");
#endif
      strcpy(buffer, input_buffer);
      input_buffer[0] = '\0';
#if DEBUG_SERIAL_LISTEN > 0
      DebugLnCC("Done");
#endif
      return true;
    } else {
      // These lines don't compile and I don't know why!
#if DEBUG_SERIAL_LISTEN > 0
      DebugLnCC("Available bytes:\n");
      DebugLn<const int>(Serial.available());
#endif
      return false;
    }
  }

  return new_from_UI;
#endif
} // Listening to the communication channel
