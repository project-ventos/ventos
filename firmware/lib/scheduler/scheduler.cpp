#ifndef ARDUINO
#include <chrono>
#include <iostream>
#else
#include <Arduino.h>
#endif
#include <debug.h>
#include <scheduler.h>
#include <task.h>
#include <timer.h>

namespace VentOS {

// Trigger is set true to enable the next task
// to be run on the next Arduino loop() call
volatile bool trigger = false;

// This is a hardware timer interrupt for ESP32 only
#ifdef ESP32HW
// Thanks to: https://diyprojects.io/esp32-timers-alarms-interrupts-arduino-code
// int totalInterrupts;
hw_timer_t *hwTimer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

// Critical code that runs in RAM of the ESP32
void IRAM_ATTR onTime() {
  portENTER_CRITICAL_ISR(&timerMux);
  trigger = true;
  portEXIT_CRITICAL_ISR(&timerMux);
}
#endif

// bool TaskScheduler::setTask(int index, Task *task){
//   DebugCC("setTask: ");
//   DebugLn<int>(index);
//   queue[index] = task;
//   return true;
// }

bool TaskScheduler::addTask(Task *task) {
  DebugLnCC("addTask: ");
  if (NUM_TASKS < MAX_TASKS) {
    queue[NUM_TASKS] = task;
    NUM_TASKS++;
    DebugLn(NUM_TASKS);
    DebugLnCC("Done adding! ");
    return true;
  } else {
    return false;
  }
}

int TaskScheduler::setTimeslice(int t) { // ms
  return timeslice = t;
}

int TaskScheduler::setSchedulerMode(int mode) { return schMode = mode; }

#define DEBUG_RUNNER 0
bool TaskScheduler::runNextTask() {
#if DEBUG_RUNNER > 0
  DebugCC("Running task index: ");
  DebugLn<uint32_t>(qIndex);
  DebugCC("NUM_TASKS: ");
  DebugLn<uint32_t>(NUM_TASKS);
#endif

  //    if (qIndex >= 0){ // && < NUM_TASKS
#ifdef ESP32HW
  queue[qIndex]->run(hwTimer.elapsed());
#else
  queue[qIndex]->run(swTimer.elapsed());
#endif
  //    }

  // Debug<const char*>("qIndex: ");
  // DebugLn<int>(qIndex);
#ifdef DEBUG_RUNNER
#ifdef ESP32HW
  DebugLn<int>(hwTimer.elapsed());
#else

#if DEBUG_RUNNER > 0
  DebugCC("swTimer: ");
  DebugLn<uint32_t>(swTimer.elapsed());
#endif

#endif
#endif

  qIndex = (qIndex + 1) % NUM_TASKS;

  return true;
}

/* Called every loop() by main.cpp */
bool TaskScheduler::loop() {

  if (schMode == 0) {
#ifndef ESP32HW
    swTimer.update();
#endif
    runNextTask();
  } else if (schMode == 1) {
#ifndef ESP32HW // default embedded software timer
    if (swTimer.elapsed() % timeslice >=
        (timeslice - 1)) { // TODO: fix this timing!
      trigger = true;
    }
#endif

    if (trigger) {
#ifdef ESP32HW
      portENTER_CRITICAL(&timerMux);
      trigger = false;
      portEXIT_CRITICAL(&timerMux);
#else
      trigger = false;
#endif

      runNextTask();
    }
  } else {
    DebugLnCC("Scheduler mode not valid!");
    return false;
  }

  return true;
}

bool TaskScheduler::start() {
  DebugLnCC("Starting task scheduler");

#ifdef ESP32HW
  // This is an interrupt driven sempaphore that controls access
  //  to the task scheduler for the ESP32 environment only.

  // Configure Prescaler to 80, as our timer runs @ 80Mhz
  // Giving an output of 80,000,000 / 80 = 1,000,000 ticks / second
  hwTimer = timerBegin(0, 80, true);
  timerAttachInterrupt(hwTimer, &onTime, true); // onTime is called on interrupt

  // Fire interrupt every fraction of a second (1s = 1 million ticks)
  // timeslice must be set before calling start!
  timerAlarmWrite(hwTimer, 1000 * timeslice, true);
  timerAlarmEnable(hwTimer);
#else

#endif

  // Call setup functions
  for (int i = 0; i < NUM_TASKS; i++) {
    queue[i]->setup();
  }

  return true;
}

} // namespace VentOS
