#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <inttypes.h>
#include <task.h>
#include <timer.h>

namespace VentOS {

// Note: Simply changing this to 4 makes the UNO
// crash in the way it does when it has a stack crash.
// I have not explanation for this.
#define MAX_TASKS 10

class TaskScheduler {
private:
  int NUM_TASKS = 0;
  uint32_t timeslice; // used when schMode == 1
  int qIndex = 0;
  int queueLength;
  int schMode; // 0 = run as fast as possible, 1 = fixed timeslice
public:
  // This is needed in other places---quite
  Timer swTimer;
  Task *queue[MAX_TASKS];
  TaskScheduler() { // TODO: add num_tasks to the constructor
#ifdef ARDUINO
#ifndef ESP32
    swTimer = Timer(millis()); // default embedded software timer
#endif
#else
    swTimer = Timer(timeSinceEpochMs()); // native software timer
#endif

    qIndex = 0;
    schMode = 0;
    NUM_TASKS = 0;
  }

  // TODO: these should be unit tested
  bool addTask(Task *task);
  //        bool setTask(int index, Task *task);
  bool start();
  bool loop();
  bool runNextTask();
  int setSchedulerMode(int mode);
  int setTimeslice(int t);
};

} // namespace VentOS

#endif
