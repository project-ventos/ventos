#ifndef DEBUG_H
#define DEBUG_H

#ifdef ARDUINO
#include <Arduino.h>
#else
#include <iostream>
#include <logger.h>
#endif

// Note: This is throwing a warning that F is redefined
// when builiding for the ESP32 (because they already
// define it as a utility. My #if is not working here!

#ifdef DEFINE_EMPTY_F_MACRO
#define F(string_literal) string_literal
#define DebugLnCC(string_literal) DebugLn(string_literal);
#define DebugCC(string_literal) Debug(string_literal);
#else
#ifdef DEFINE_F_MACRO
#define F(string_literal)                                                      \
  (reinterpret_cast<const __FlashStringHelper *>(PSTR(string_literal)))
#define DebugLnCC(string_literal)                                              \
  DebugLn<const __FlashStringHelper *>(F(string_literal))
#define DebugCC(string_literal)                                                \
  Debug<const __FlashStringHelper *>(F(string_literal));
#else
#define DebugLnCC(string_literal) DebugLn(string_literal)
#define DebugCC(string_literal) Debug(string_literal);
#endif
#endif

int freeMemory();

namespace VentOS {

// For example, call Debug<const char*>("Some text") or Debug<bool>(myBoolVar)
// to get a debug output on Arduino or native environments
template <class myType> inline void Debug(myType a) {
#ifdef ARDUINO
  Serial.print(a);
#else
  std::cout << a;
  vos_log(a);
#endif
}

// The same as Debug but adds a line break.
template <class myType> inline void DebugLn(myType a) {
#ifdef ARDUINO
  Serial.println(a);
#else
  std::cout << a << std::endl;
  vos_log(a);
#endif
}
} // namespace VentOS

#endif
