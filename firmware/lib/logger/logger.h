#ifndef LOGGER_H
#define LOGGER_H
#include <fstream>
#include <iostream>
using namespace std;

template <class myType> void vos_log(myType txt) {
  try {
    ofstream fs;
    fs.open("log.txt", ios_base::app);
    if (!fs.is_open()) {
      throw("Couldn't open log file!");
    }
    fs << txt << endl;
    fs.close();
  } catch (const char *errMsg) {
    cout << errMsg << endl;
  }
}

#endif
