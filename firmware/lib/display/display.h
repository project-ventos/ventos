#ifndef DISPLAY_H
#define DISPLAY_H

#include <PIRDS.h>
#include <task.h>

namespace VentOS {

// The display implementation should inherit from this class
class DisplayController : public Task {
public:
  DisplayController() {}
  bool setup();
  bool run(uint32_t msNow);
  virtual bool connect();
  virtual bool sendMessage(Message m);
  virtual bool sendMeasurement(Measurement m);
  // TODO: touch screen/button inputs? Maybe in another class
};

} // namespace VentOS

#endif