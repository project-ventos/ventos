#include <PIRDS.h>
#include <debug.h>
#include <display.h>
#include <task.h>

// #define DEBUG 0
namespace VentOS {

bool DisplayController::setup() {
  DebugLnCC("Display controller setup");
  return true;
}

bool DisplayController::run(uint32_t msNow) {
#ifdef DEBUG
  DebugLnCC("Display controller run");
#endif
  return true;
}

bool DisplayController::connect() { return true; }

bool DisplayController::sendMessage(Message m) { return true; }

// Send PIRDS measurement to display.
bool DisplayController::sendMeasurement(Measurement m) { return true; }

} // namespace VentOS
