#ifndef VOS_CONFIG_MOCK_H
#define VOS_CONFIG_MOCK_H

#include <controller.h>
#include <sensor.h>
#include <vos_simulator.h>

// using namespace VOS_Controller;

// This will likely be redefinition, but that is okay...
#define SENSOR_CNT 2

extern sensor SENSORS[];

unsigned long simulatedPressureReading(bool);

void set_ventilator_state(VentStateT *v, VentSettingsT *vset);

#endif
