#include <debug.h>
#include <drive_simulator.h>

namespace VentOS {

// return number is expected flow in milliliters per second that the air drive
// thinks it can return flow_mlps is the require flow in milliliters pers second
// at_pressure_cmH2O_tenths is the pressure in the mm of water against which the
// air drive must operate duration_ms is the time that the this flow should be
// produced in ms from_now_ms is when to begin producing this flow. This can be
// used to allow the drive to prepare for action. client_data is a pointer to a
// 64 byte buffer guaranteeed to be returned on the next call undisturbed.
// custom is a pointer to an character buffer of unspecified size. It is used
// only for data specific to the given airdrive.
uint32_t SimulatorDrive::drive_flow(uint32_t flow_mlps,
                                    uint32_t at_pressure_cmH2O_tenths,
                                    uint32_t max_pressure_cmH2O_tenths,
                                    char *client_data, const char *custom) {

  return flow_mlps;
}

uint32_t SimulatorDrive::drive_pressure(uint32_t flow_mlps,
                                        uint32_t at_pressure_cmH2O_tenths,
                                        uint32_t max_pressure_cmH2O_tenths,
                                        char *client_data, const char *custom) {
  ((uint32_t *)client_data)[0] = at_pressure_cmH2O_tenths;

  // Being a simulation, this is a "perfect" drive...
  return at_pressure_cmH2O_tenths;
}

uint32_t SimulatorDrive::pause_and_prep(uint32_t timer_ms, char *client_data,
                                        const char *custom) {
  Debug<const char *>("driver pausing and preping\n");

  // Send the data over SPI
  // ...
  // ...
  Debug<const char *>("pause_and_prep called: ");
  Debug<uint32_t>(timer_ms);
  // Just to be sneaky, I will add the volume produced since the last
  // pause_and_prep call to my client_data!
  ((long *)client_data)[0] = 0;
  return timer_ms;
}

uint32_t SimulatorDrive::setFiO2(uint32_t FiO2_per_cent_times_ten) { return 0; }

uint32_t SimulatorDrive::get_version() { return DRIVE_SIMULATOR_VERSION; }

} // namespace VentOS
