#ifndef DRIVE_SIMULATOR_UNO_H
#define DRIVE_SIMULATOR_UNO_H

#include <drive.h>
#include <inttypes.h>

namespace VentOS {

class SimulatorUnoDrive : public Drive {
public:
  uint32_t drive_flow(uint32_t flow_mlps, uint32_t at_pressure_cmH2O_tenths,
                      uint32_t max_pressure_cmH2O_tenths, char *client_data,
                      const char *custom);
  uint32_t drive_pressure(uint32_t flow_mlps, uint32_t at_pressure_cmH2O_tenths,
                          uint32_t max_pressure_cmH2O_tenths, char *client_data,
                          const char *custom);

  uint32_t pause_and_prep(uint32_t duration_ms, char *client_data,
                          const char *custom);
  uint32_t get_version();
};
} // namespace VentOS

#endif
