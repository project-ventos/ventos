#ifdef INCLUDE_POLYVENT_CONFIG_PV

// NOTE: This is highly specific to the PolyVent project
// and shoule only be included by conditional compilation.
// The PolyVent board is an ESP32 that communicates with
// Arduino Nanos that drive the stepper motors in the bellows.
// These are connected via SPI. So, basically, these
// functions are going to make SPI calls.

// Exactly what the form of the API accessed via these SPI
// calls will be is still being determined.
// One option is that it is a "raw" stepper motor interface;
// Another option is that essentially a pass through of these
// functions, and that the real work is done on the Nano.

#if defined(INCLUDE_POLYVENT_CONFIG_PV)
#include <drive.h>

#include <polyvent_drive_pv.h>
// #include <debug.h>

// POLYVENT-specific code from Nathaniel
#include <Arduino.h>
#include <HoneywellTruStabilitySPI.h>
#include <SPI.h>

// The drive specifically uses a PID Controller to implement
// a pressure controlled ventilation mode. We will open
// or close the valve based on the pressure we are reading.
// The process variable will be the pressure, and the
// plant variable will be the voltage to the proportional valve.
// https://en.wikipedia.org/wiki/PID_controller
#include <PID_v1.h>
#include <SFM3X00.h>

#define DEBUG_LEVEL 0

#define DEBUG_VALVE_BOARD 0

// WARNING!! For some unknown reason, probably related to timing,
// the system fails dangerously if DEBUG_PID is zero!!!
#define DEBUG_PID 0

#define DEBUG_OPEN_VALVES 0

#define DEBUG_CHAMBER 0

// This needs to be moved up or into a utility class
void send_uint16_t(uint16_t u) {

  //#if defined(INCLUDE_POLYVENT_CONFIG)
  SPI.transfer(*(((uint8_t *)&u) + 0));
  //  uint8_t b1 = *(((uint8_t *)&u) + 0);

  SPI.transfer(*(((uint8_t *)&u) + 1));
  //  uint8_t b2 = *(((uint8_t *)&u) + 1);
  //#endif
}

// from
// https://www.codeproject.com/Articles/36459/PID-process-control-a-Cruise-Control-example
// Note: This insight from the above is critical:
// ProcessValue = ProcessValue + (output * efficiency) – loss
double plant1(double process, double currentPV) {
  /* this is my version of cruise control.
   * PV = PV + (output * .2) - (PV*.10);
   * The equation contains values for speed, efficiency,
   * and wind resistance.
   * Here 'PV' is the speed of the car.
   * 'output' is the amount of gas supplied to the engine.
   * (It is only 20% efficient in this example)
   * And it looses energy at 10% of the speed. (The faster the
   * car moves the more PV will be reduced.)
   * Noise is added randomly if checked, otherwise noise is 0.0
   * (Noise doesn't relate to the cruise control, but can be useful
   * when modeling other processes.)
   */
  const float RETAINED = 0.8;
  double PV = RETAINED * currentPV + (process * (1.0 - RETAINED));
  // change the above equation to fit your application.
  return PV;
}

namespace VentOS {

PolyVentDrivePV::PolyVentDrivePV() {
  // These will actually have to be tuned, this has not yet been done..

  // I wish PID controllers in general made it clearere what
  // the units of these values are. These were discovered by
  // trial-and-error, which is typical of PID controllers, but
  // very annoying.
  // Original, working values...
  // double PKp = 0.01;
  // double PKi = 0.0;
  // double PKd = 0.001;

  double PKp = 0.05;
  // Note. any integrative value at all tends to fail,
  // probably because of how quickly we move...
  double PKi = 0.0000;
  double PKd = 0.001;

  // These values are untested.
  double FKp = 0.2;
  double FKi = 0.0;
  double FKd = 0.0;
  const int SAMPLE_TIME_MS = 1;

  this->pidControllerPressure = new PID(
      &(this->Input_cm_H2O), &(this->Pressure_Output_duty_cycle_per_cent),
      &(this->Pressure_Setpoint_cm_H2O), PKp, PKi, PKd, DIRECT);

  this->pidControllerPressure->SetOutputLimits(-100.0, 100.0);
  this->pidControllerPressure->SetSampleTime(SAMPLE_TIME_MS);
  this->pidControllerPressure->SetMode(AUTOMATIC);

  this->pidControllerFlow =
      new PID(&(this->Input_mlps), &(this->Flow_Output_duty_cycle_per_cent),
              &(this->Flow_Setpoint_mlps), FKp, FKi, FKd, DIRECT);

  this->pidControllerFlow->SetOutputLimits(-100.0, 100.0);
  this->pidControllerFlow->SetSampleTime(SAMPLE_TIME_MS);
  this->pidControllerFlow->SetMode(AUTOMATIC);
}

// This code is highly specific to the PolyVent V2.0 controller board.
// At the time of this writing this is a 13 word message which contains
// the basic PWM frequency (in position 0), and the 12 other PWM values as
// unsigned 16 bit values. 65535 means "HIGH" and 0 means "LOW".
// In the drivery code, the value for each of the 12 PWM values is mapped
// from the range 0-65535 into a dutycyle between 0% and 100%.
void PolyVentDrivePV::send_full_buffer_to_controller_board(
    float pv_duty_cycle_per_cent, bool inhaling, bool air_open, bool o2_open) {

  if (DEBUG_VALVE_BOARD > 1) {
    DebugLn("SENDING BUFFER ROUTINE ");
  }
  const uint16_t MAX_UINT16 = 65535;

  float effective_duty_cycle =
      (pv_duty_cycle_per_cent < 0 ? 0.0 : pv_duty_cycle_per_cent);

  uint16_t dc = (effective_duty_cycle / 100.0) * MAX_UINT16;

  // These are experimental values for the IQ-Valve with the smaller oriface
  // (0.281)
  const uint16_t MAXIMUM_VALVE_RANGE_VALUE = 65535;
  const uint16_t MINIMUM_VALVE_RANGE_VALUE = 0;

  uint16_t adjusted_dc;
  if (!inhaling) {
    adjusted_dc = 0;
  } else {
    adjusted_dc =
        map(dc, 0, 65535, MINIMUM_VALVE_RANGE_VALUE, MAXIMUM_VALVE_RANGE_VALUE);
  }

  if (DEBUG_PID > 4) {
    DebugCC("inhaling: ");
    DebugLn(inhaling);
  }
  // else {
  //   delay(1);
  // }

  if (DEBUG_PID > 4) {
    DebugCC("proportional value: (pre,mapped) ");
    DebugCC(dc);
    DebugCC(",");
    DebugLn(adjusted_dc);
  }
  // else {
  //   delay(1);
  // }

  uint16_t value = inhaling ? 0 : MAX_UINT16;
  if (DEBUG_PID > 3) {
    DebugCC("PIV value: ");
    DebugLn(value);
  }
  // else {
  //   delay(1);
  // }

  // NOTE: This block needs to be executed quickly. Do not put
  // slow debugging statements inside this SPI transmission block.

  digitalWrite(this->ESP_CS_PINS[VALVES_BOARD_CS_IDX], LOW);
  send_uint16_t(this->PWM_FREQUENCY);

  if (DEBUG_OPEN_VALVES > 0) {
    air_open = 1;
  }
  if (DEBUG_OPEN_VALVES > 0) {
    o2_open = 1;
  }

  if (DEBUG_VALVE_BOARD > 1) {
    DebugCC("AIR OPEN ");
    DebugLn(air_open);
    DebugCC("O2 OPEN ");
    DebugLn(o2_open);
  }
  for (int x = 1; x <= 12; x++) {
    switch (x) {
    case PolyVentDrivePV::RESPIRATION_VALVE:
      send_uint16_t(adjusted_dc);
      break;
    case PolyVentDrivePV::PATIENT_INFLATING_VALVE: {
      send_uint16_t(value);
      break;
    }
    case PolyVentDrivePV::AIR_INPUT_VALVE:
      send_uint16_t(air_open ? MAX_UINT16 : 0); // this is the duty cycle
      break;
    case PolyVentDrivePV::O2_INPUT_VALVE:
      send_uint16_t(o2_open ? MAX_UINT16 : 0);
      break;
    default:
      send_uint16_t(0); // There are really "DONT CARE" values
    }
  }
  digitalWrite(this->ESP_CS_PINS[VALVES_BOARD_CS_IDX], HIGH);

  if (DEBUG_VALVE_BOARD > 1) {
    DebugLn("DATA_SENT! ");
  }
}

// return number is expected flow in milliliters per second that the air drive
// thinks it can return flow_mlps is the require flow in milliliters pers second
// at_pressure_cmH2O_tenths is the pressure in the mm of water against which the
// air drive must operate duration_ms is the time that the this flow should be
// produced in ms from_now_ms is when to begin producing this flow. This can be
// used to allow the drive to prepare for action. client_data is a pointer to a
// 64 byte buffer guaranteeed to be returned on the next call undisturbed.
// custom is a pointer to an character buffer of unspecified size. It is used
// only for data specific to the given airdrive.

const float CONVERT_SLM_TO_MLPS = 60.0 * 1000.0;

void PolyVentDrivePV::begin_inhalation_phase() {
  IS_CHAMBER_LOADED_FOR_THIS_BREATH = false;
  IS_CHAMBER_FILLING = false;
  IS_CHAMBER_FILLING_WITH_O2 = false;
}
uint32_t PolyVentDrivePV::drive_flow(uint32_t flow_mlps,
                                     uint32_t at_pressure_cmH2O_tenths,
                                     uint32_t max_pressure_cmH2O_tenths,
                                     char *client_data, const char *custom) {
  begin_inhalation_phase();

  // read flow from sensor and print
  float flow = flowSensor->readFlow();

  if (flowSensor->checkRange(flow)) {
    Serial.print("flow exceeded sensor limits:  ");
    Serial.print(flow);
    Serial.println(" slm");
  } else {
    Serial.print("flow : ");
    Serial.print(flow);
    Serial.println(" slm");
  }

  //  delay(10);
  float f_slm = 1.0;
  double f_mlps = CONVERT_SLM_TO_MLPS * f_slm;
  if (flowSensor->checkRange(flow)) {
    Serial.print("flow exceeded sensor limits:  ");
    Serial.print(flow);
    Serial.println(" slm");
    Serial.println("This is a severe error");
    return 0;
  } else {
    const int USE_TEST_MODE = 0;
    if (!USE_TEST_MODE) {
      f_slm = flow;
      f_mlps = CONVERT_SLM_TO_MLPS * f_slm;

      if (DEBUG_PID > 0) {
        DebugCC("measured  flow (f_mlps): ");
        DebugLn(f_mlps);
      }
      // else {
      //   delay(1);
      // }
    } else {
      // In order to test our use of the PID controller before giving this to
      // Nathaniel to test, we have to have some model of how the duty cycle
      // impacts pressure. This is impossible to know for sure, but we can
      // create a linear parametrized model. Let's assume 80% open would provide
      // a full 70cmH2O, which would be way too high for most clinical
      // situations. Then we will use a simple map to compute the amount.
      // I'm going to assume that in general fully open will be
      // is enough to give us 800ml in a 2 second input.
      // So 100% open give us 400 mlps.
      const float CONVERT_DF_TO_MLPS = 800.0 / 2.0;

      float new_value =
          (this->Flow_final_process_per_cent / 100.0) * CONVERT_DF_TO_MLPS;
      if (DEBUG_PID > 0) {
        DebugCC("new_value (per cent): ");
        DebugLn(new_value);
      }
      // else {
      //   delay(1);
      // }

      f_mlps = plant1(new_value, this->Flow_final_process_per_cent);
    }
    // Now basically f_mlps is our process variable, and the current duty cycle
    // for the proportional valve is our plant variable.
    this->Input_mlps = f_mlps;
    if (DEBUG_PID > 0) {
      DebugCC("Input flow (ml per second): ");
      DebugLn(this->Input_mlps);
    }
    // else {
    //   delay(1);
    // }
    this->Flow_Setpoint_mlps = flow_mlps;
    double previousInput = this->Input_mlps;
    pidControllerFlow->Compute();
    // I believe this pidController is particularly weird, instead
    // of outputing the process value, it outputs the delta to the PV.
    if (DEBUG_PID > 0) {
      DebugCC("Flow_OutputDutyCycle per cent: ");
      DebugLn(this->Flow_Output_duty_cycle_per_cent);
    }
    // else {
    //   delay(1);
    // }
    this->Flow_final_process_per_cent = this->Flow_final_process_per_cent +
                                        this->Flow_Output_duty_cycle_per_cent;
    // Now we clamp the final_process_per_cent to be between 100.0 and -100.0...
    // even though negative values are not possible.
    this->Flow_final_process_per_cent =
        min(this->Flow_final_process_per_cent, 100.0);
    this->Flow_final_process_per_cent =
        max(this->Flow_final_process_per_cent, -100.0);

    if (DEBUG_PID > 0) {
      DebugCC("Flow_final_process per cent: ");
      DebugLn(this->Flow_final_process_per_cent);
    }
    // else {
    //   delay(1);
    // }
    send_full_buffer_to_controller_board(this->Flow_final_process_per_cent,
                                         true, false, false);
  }

  ((uint32_t *)client_data)[0] = (uint32_t)flow_mlps;

  return (uint32_t)flow_mlps;
}

const float CONVERT_PSI_TO_CM_H2O = 70.307;
const float CONVERT_MBAR_TO_CM_H20 = 1.01972;
// Convert Standard Liters per Minutes to ml per second

float convert_psi_to_mm_h2o(float p_psi) {
  uint32_t measured_p = (uint32_t)p_psi * CONVERT_PSI_TO_CM_H2O * 10.0;
  return measured_p;
}

uint32_t PolyVentDrivePV::drive_pressure(uint32_t flow_mlps,
                                         uint32_t at_pressure_cmH2O_tenths,
                                         uint32_t max_pressure_cmH2O_tenths,
                                         char *client_data,
                                         const char *custom) {
#ifdef INCLUDE_POLYVENT_CONFIG_PV

  // WARNING! Calling readSensor() on these seems to hang
  // if the pressure sensor is not present. These should
  // probably be predicted with some kind of timeout, but
  // that is a low priority at present.

  // NOTE: The PolyVent has 6 ports, which could be physically
  // routed to different positions. We are assuming that
  // sensor0 (which is labeled as Port 1) ont he machine)
  // is attached to the airway

  begin_inhalation_phase();
  float p_mbar = 1.0;
  double p_cm_H2O = CM_H2O_PER_PSI;
  if (this->sensor2.readSensor() == 0) {
    const int USE_TEST_MODE = 0;
    if (!USE_TEST_MODE) {
      p_mbar = this->sensor2.pressure();
      p_cm_H2O = p_mbar * CONVERT_MBAR_TO_CM_H20;

      if (DEBUG_PID > 0) {
        DebugCC("PVP measured  pressure (cm_H2O): ");
        DebugLn(p_cm_H2O);
      }
    } else {
      // In order to test our use of the PID controller before giving this to
      // Nathaniel to test, we have to have some model of how the duty cycle
      // impacts pressure. This is impossible to know for sure, but we can
      // create a linear parametrized model. Let's assume 80% open would provide
      // a full 70cmH2O, which would be way too high for most clinical
      // situations. Then we will use a simple map to compute the amount.
      const float CONVERT_DF_TO_CM_H2O = 70.0 / 0.80;
      //      int new_value = map((int)
      //      this->Output_duty_cycle_per_cent,0,100,0,(int)
      //      CONVERT_DF_TO_CM_H2O);

      float new_value = (this->Pressure_final_process_per_cent / 100.0) *
                        CONVERT_DF_TO_CM_H2O;
      if (DEBUG_PID > 1) {
        DebugCC("new_value (per cent): ");
        DebugLn(new_value);
      }
      p_cm_H2O = plant1(new_value, this->Pressure_final_process_per_cent);
    }
    //    DebugLn(p_psi);
    // Now basically p_psi is our process variable, and the current duty cycle
    // for the proportional valve is our plant variable.
    this->Input_cm_H2O = p_cm_H2O;
    if (DEBUG_PID > 1) {
      DebugCC("Input pressure (cm_H2O): ");
      DebugLn(this->Input_cm_H2O);
    }
    this->Pressure_Setpoint_cm_H2O = at_pressure_cmH2O_tenths / 10.0;
    if (DEBUG_PID > 0) {
      DebugCC("Pressure SetPoint cm_H2O: ");
      DebugLn(this->Pressure_Setpoint_cm_H2O);
    }

    double previousInput = this->Input_cm_H2O;
    pidControllerPressure->Compute();
    // I believe this pidController is particularly weird, instead
    // of outputing the process value, it outputs the delta to the PV.
    if (DEBUG_PID > 1) {
      DebugCC("Pressure OutputDutyCycle per cent: ");
      DebugLn(this->Pressure_Output_duty_cycle_per_cent);
    }
    this->Pressure_final_process_per_cent =
        this->Pressure_final_process_per_cent +
        this->Pressure_Output_duty_cycle_per_cent;
    // Now we clamp the final_process_per_cent to be between 100.0 and -100.0...
    // even though negative values are not possible.
    this->Pressure_final_process_per_cent =
        min(this->Pressure_final_process_per_cent, 100.0);
    this->Pressure_final_process_per_cent =
        max(this->Pressure_final_process_per_cent, -100.0);

    if (DEBUG_PID > 1) {
      DebugCC("Pressure final_process per cent: ");
      DebugLn(this->Pressure_final_process_per_cent);
    }
    send_full_buffer_to_controller_board(this->Pressure_final_process_per_cent,
                                         true, false, false);
  } else {
    if (DEBUG_LEVEL > 2) {
      DebugLnCC("Pressure sensor not ready !");
      return 0;
    }
  }

#endif
  ((uint32_t *)client_data)[0] = at_pressure_cmH2O_tenths;
  // Being a simulation, this is a "perfect" drive...
  return at_pressure_cmH2O_tenths;
}

// NOTE: In the case of PolyVent, pause_and_prep may be
// especially important, because the bellows have to return to
// a "home" position before the next breath begins, which takes
// some time. Additionally to this, this phase is used by the bellows
// to do oxygen mixing, a critical feature of PolyVent.
uint32_t PolyVentDrivePV::pause_and_prep(uint32_t timer_ms, char *client_data,
                                         const char *custom) {
  //    Debug<const char*>("driver pausing and preping\n");

  // Noted --- read rawSensor() on this.
  //
#if defined(INCLUDE_POLYVENT_CONFIG_PV)

  const float BAR_PER_PSI = 0.0689476;
  if (!IS_CHAMBER_LOADED_FOR_THIS_BREATH) {
    if (this->sensor3.readSensor() == 0) {
      float t = this->sensor3.temperature();
      float pressure = this->sensor3.pressure();
      float pressure_bar = pressure * BAR_PER_PSI;
      //      float rawPressure = this->sensor3.rawPressure();
      if (DEBUG_CHAMBER > 0) {
        DebugLnCC("pressure!");
        DebugLn(pressure);
        //        DebugLnCC("rawPressure!");
        //        DebugLn(rawPressure);
        DebugLnCC("tempo!");
        DebugLn(t);

        DebugLnCC("IS_CHAMBER_FILLING");
        DebugLn(IS_CHAMBER_FILLING);

        DebugLnCC("TARGET_CHAMBAR_PRESSURE_BAR");
        DebugLn(TARGET_CHAMBER_PRESSURE_BAR);
      }
      if (!IS_CHAMBER_FILLING) {
        CHAMBER_DELTA_TO_FILL = TARGET_CHAMBER_PRESSURE_BAR - pressure_bar;
        TARGET_OXYGEN_PRESSURE_BAR =
            pressure_bar + TARGET_FI_O2 * CHAMBER_DELTA_TO_FILL;
        IS_CHAMBER_FILLING = true;
        IS_CHAMBER_FILLING_WITH_O2 = true;
        if (DEBUG_CHAMBER > 0) {
          DebugLnCC("Beginning O2 Fill!");
          DebugLnCC("TARGET_OXYGEN_PRESSURE_BAR");
          DebugLn(TARGET_OXYGEN_PRESSURE_BAR);
        }
      }

      if (IS_CHAMBER_FILLING_WITH_O2 &&
          (pressure < TARGET_OXYGEN_PRESSURE_BAR)) {
        send_full_buffer_to_controller_board(0.0, false, false, true);
      } else {
        send_full_buffer_to_controller_board(0.0, false, true, false);
        IS_CHAMBER_FILLING = true;
        IS_CHAMBER_FILLING_WITH_O2 = false;
        if (DEBUG_CHAMBER > 0) {
          DebugLnCC("Ending O2 Fill!");
        }
      }

      if (DEBUG_CHAMBER > 0) {
        DebugLnCC("pressure (bar)!");
        DebugLn(pressure_bar);
      }
      if (!IS_CHAMBER_FILLING_WITH_O2) {
        if (pressure_bar < TARGET_CHAMBER_PRESSURE_BAR) {
          send_full_buffer_to_controller_board(0.0, false, true, false);
          if (DEBUG_CHAMBER > 0) {
            DebugLnCC("Filling with Air!");
          }
        } else {
          send_full_buffer_to_controller_board(0.0, false, false, false);
          IS_CHAMBER_LOADED_FOR_THIS_BREATH = true;
          IS_CHAMBER_FILLING = false;
          if (DEBUG_CHAMBER > 0) {
            DebugLnCC("DONE FILLING THIS BREATH!");
          }
        }
      }
    }
  }
  if (!expiration_valves_set) {
    inspiration_valves_set = false;
    expiration_valves_set = true;
  }

#endif

  return timer_ms;
}

void PolyVentDrivePV::home_machine() {
  if (DEBUG_LEVEL > 0) {
    DebugLnCC("HOMING MACHINE");
  }
}

uint32_t PolyVentDrivePV::get_version() { return POLYVENT_VERSION; }

// For the PolyVentPV machine, an emergency stop requires us
// to shut the mixing and intake valves, and open the PIV valve.
// This is the same protocol as the "pause_and_prep" protocol,
// except that
uint32_t PolyVentDrivePV::emergency_stop() {
  send_full_buffer_to_controller_board(0.0, false, false, false);
  DebugLnCC("EMERGNECY_STOP executed!");
  return 0;
};
// This drive does not support FiO2 control
uint32_t PolyVentDrivePV::setFiO2(uint32_t FiO2_per_cent_times_ten) {
  this->TARGET_FI_O2 = FiO2_per_cent_times_ten / 1000.0;
  return 0;
}

} // namespace VentOS

#endif

#endif
