#ifndef DRIVE_H
#define DRIVE_H

#include <inttypes.h>

// These are just magic numbers to prove we are reading
// the correct version.
#define DRIVE_SIMULATOR_VERSION 357
#define DRIVE_SIMULATOR_UNO_VERSION 222
#define POLYVENT_VERSION 835

namespace VentOS {

class Drive {
public:
  char *client_data;
  int client_data_size;
  virtual uint32_t drive_flow(uint32_t flow_mlps,
                              uint32_t at_pressure_cmH2O_tenths,
                              uint32_t max_pressure_cmH2O_tenths,
                              char *client_data, const char *custom) {
    return flow_mlps;
  };
  virtual uint32_t drive_pressure(uint32_t flow_mlps,
                                  uint32_t at_pressure_cmH2O_tenths,
                                  uint32_t max_pressure_cmH2O_tenths,
                                  char *client_data, const char *custom) {
    return flow_mlps;
  };

  virtual uint32_t pause_and_prep(uint32_t duration_ms, char *client_data,
                                  const char *custom) {
    return 0;
  };

  // a non-zero value means the value is out of range.
  virtual uint32_t setFiO2(uint32_t FiO2_per_cent_times_ten) { return 0; }
  virtual uint32_t get_version() { return 0; };

  // perform an emergency stop. Return a non-zero error code
  // if the emergency stop failed.
  // Some hardware need do nothing for an emergency stop,
  // but some hardware needs to take definite action.
  virtual uint32_t emergency_stop() { return 0; };
};

} // namespace VentOS

#endif
