#ifndef OVB_DRIVE_H
#define OVB_DRIVE_H

#include <debug.h>
#include <drive.h>
#include <inttypes.h>

#if defined(INCLUDE_OVB_CONFIG)
#include <PID_v1.h>

namespace VentOS {

// Rename to PolyVentII --- this is more generic.
// the PolyVent has a "Control Module" which is quite generice.
// In a perfect world we would reflect this distinction in our class
// strcture
class OVBDrive : public Drive {

private:
public:
  // We need a constructor for some initialization
  OVBDrive();

  PID *pidControllerPressure;
  PID *pidControllerFlow;

  const uint16_t PWM_FREQUENCY =
      200; // NOTE: I don't know what are typical valuves here!

  // the POLYVVENT II hardware defines 4 vavles:
  // https://gitlab.com/polyvent/polyvent_control_module/-/wikis/4.2-Proportional-Valve-Ventilator-Configuration
  // These correspond to the pins on the controller board,
  // which correspond to the position in the tranmission block we
  // send.
  // Valve#1 = Air Input (On/Off)
  // Valve#2 = O2 Input (On/Off)
  // Valve#3 = Respiration Valve (proportional)
  // Valve#4 = Patient Inflating Valve (On/Off)
  static const int AIR_INPUT_VALVE = 1;
  static const int O2_INPUT_VALVE = 2;
  static const int RESPIRATION_VALVE = 3;
  static const int PATIENT_INFLATING_VALVE = 4;

  bool IS_CHAMBER_LOADED_FOR_THIS_BREATH = false;
  bool IS_CHAMBER_FILLING = false;
  bool IS_CHAMBER_FILLING_WITH_O2 = false;
  float TARGET_FI_O2 = 0.5;

  const float TARGET_CHAMBER_PRESSURE_BAR = 1.5;
  float TARGET_OXYGEN_PRESSURE_BAR;
  float CHAMBER_DELTA_TO_FILL = 0.0;

  const int SPI_TRANSMISSION_PIN = 4;

  // For Pressure Mode
  const double CM_H2O_PER_PSI = 70.307;
  double Pressure_Setpoint_cm_H2O = 0;
  double Input_cm_H2O = 0;
  double Pressure_Output_duty_cycle_per_cent = 0.0;
  double Pressure_final_process_per_cent = 0.0;

  // For Volume Mode
  double Input_mlps = 0;
  double Flow_Setpoint_mlps = 0;

  double Flow_Output_duty_cycle_per_cent = 0.0;
  double Flow_final_process_per_cent = 0.0;

  void begin_inhalation_phase();

  // TODO: This code may be obsolete for the V2 boards
  const uint8_t ESP_CS_PINS[13] = {4,  5,  13, 14, 15, 16, 17,
                                   21, 22, 25, 26, 32, 33};

  const int VALVES_BOARD_CS_IDX = 3;

  boolean inspiration_valves_set = false;
  boolean expiration_valves_set = false;

  // uncomment below for constant values
  uint8_t conditionals = 0;

  // return number is expected flow in milliliters per second that the air drive
  // thinks it can return flow_mlps is the require flow in milliliters pers
  // second at_pressure_cmH2O_tenths is the pressure in the mm of water against
  // which the air drive must operate duration_ms is the time that the this flow
  // should be produced in ms from_now_ms is when to begin producing this flow.
  // This can be used to allow the drive to prepare for action. client_data is a
  // pointer to a 64 byte buffer guaranteeed to be returned on the next call
  // undisturbed. custom is a pointer to an character buffer of unspecified
  // size. It is used only for data specific to the given airdrive.
  uint32_t drive_flow(uint32_t flow_mlps, uint32_t at_pressure_cmH2O_tenths,
                      uint32_t max_pressure_cmH2O_tenths, char *client_data,
                      const char *custom);

  uint32_t drive_pressure(uint32_t flow_mlps, uint32_t at_pressure_cmH2O_tenths,
                          uint32_t max_pressure_cmH2O__tenths,
                          char *client_data, const char *custom);

  // NOTE: In the case of PolyVent, pause_and_prep may be
  // especially important, because the bellows have to return to
  // a "home" position before the next breath begins, which takes
  // some time. Additionally to this, this phase is used by the bellows
  // to do oxygen mixing, a critical feature of PolyVent.
  uint32_t pause_and_prep(uint32_t timer_ms, char *client_data,
                          const char *custom);
  // Home machine
  void home_machine();
  uint32_t get_version();
  uint32_t emergency_stop();
};
} // namespace VentOS

#endif
#endif
