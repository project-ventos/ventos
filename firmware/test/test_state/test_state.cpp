#include <inttypes.h>
#include <state.h>
#include <stdio.h>
#include <string.h>
#include <unity.h>

void test_state() {
  State vent_state;
  vent_state.set_peep = 5;
  TEST_ASSERT_EQUAL(5, vent_state.set_peep);
}

void process() {
  UNITY_BEGIN();
  RUN_TEST(test_state);
  UNITY_END();
}

#ifdef ARDUINO
#include <Arduino.h>
void setup() {
  // NOTE!!! Wait for >2 secs
  // if board doesn't support software reset via Serial.DTR/RTS
  delay(2000);
  process();
}
void loop() {
  //
}
#else
int main(int argc, char **argv) {
  process();
  return 0;
}
#endif
