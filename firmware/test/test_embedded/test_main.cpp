
#ifdef ARDUINO
#include <Arduino.h>

#include <PIRDS.h>
#include <sensor.h>
#include <sensor_honeywell.h>
#include <string.h>
#include <unity.h>

#ifdef INCLUDE_VENTMON_CONFIG
#include <vos_config_ventmon.h>
#define FOUND_CONFIG
#endif

#ifndef FOUND_CONFIG
#define SENSOR_CNT 0
sensor SENSORS[0];
#endif
// SENSORS is now defined as an array, no matter what...

// It appears that this one is not linked...
// #include <inttypes.h>

// void setUp(void) {
// // set stuff up here
// }

// void tearDown(void) {
// // clean stuff up here
// }

void setup() {
  // NOTE!!! Wait for >2 secs
  // if board doesn't support software reset via Serial.DTR/RTS

#ifdef FOUND_CONFIG
  local_setup();
#endif
  delay(2000);
  UNITY_BEGIN(); // IMPORTANT LINE!
}

void loop() {

#ifdef PLATFORM
  Serial.println("DEFINED");
#else
  Serial.println("NOT DEFINED");
#endif

#ifdef CONFIG_NAME
  Serial.println("CONFIG DEFINED");
  Serial.println(CONFIG_NAME);
#endif

#ifndef CONFIG_NAME
  Serial.println("CONFIG NOT DEFINED");
#endif

  delay(500);
  RUN_TEST(test_can_create_Measurement_and_read_back_as_byte_buffer);
  RUN_TEST(test_can_create_Measurement_and_read_back_as_JSON);
  RUN_TEST(test_can_create_Message_and_read_back_as_byte_buffer);
  RUN_TEST(test_can_create_Message_and_read_back_as_JSON);

  sensor_func differential_pressure_sensor = find_sensor(SENSORS, 'D', 'I', 0);
  if (differential_pressure_sensor) {
    Serial.println("DIFFERENTIAL PRESSURE SENSOR");

    unsigned long p = (*differential_pressure_sensor)();
    Serial.println("pressure read:");
    Serial.println(p);
    TEST_ASSERT_TRUE(p != LONG_MIN);
    //    RUN_TEST(test_differential_pressure_sensor_gives_reading);
  } else {
    Serial.println("no pressure sensors found");
  }
  UNITY_END(); // stop unit testing
}
#endif
