#include <alarm.h>
#include <debug.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <timer.h>
#include <unity.h>

using namespace VentOS_Alarm;

void test_alarm_init() {
  AlarmController a;
  AlarmState as = a.getState();
  TEST_ASSERT_EQUAL(Ok, as);
  AlarmEvent ae = a.getEvent();
  TEST_ASSERT_EQUAL(None, ae);
}

void test_alarm_reset() {
  AlarmModule a;
  TEST_ASSERT_TRUE(a.reset());
}

void test_alarm_set() {
  AlarmController a;
  AlarmState as = a.setState(Silenced);
  TEST_ASSERT_EQUAL(Silenced, as);
  as = a.setState(Low);
  TEST_ASSERT_EQUAL(Low, as);
  as = a.setState(Medium);
  TEST_ASSERT_EQUAL(Medium, as);
  as = a.setState(High);
  TEST_ASSERT_EQUAL(High, as);
  as = a.setState(Ok);
  TEST_ASSERT_EQUAL(Ok, as);
  as = a.setState(Error);
  TEST_ASSERT_EQUAL(Error, as);
}

void test_alarm_trigger() {
  AlarmModule a;
  TEST_ASSERT_TRUE(a.trigger(HIGH_PRESSURE, 431433, 600));
  /*TEST_ASSERT_EQUAL(ar.alarmEvent, HIGH_PRESSURE);
  TEST_ASSERT_EQUAL(ar.triggerTime, 431433);
  TEST_ASSERT_EQUAL(ar.triggerDuration, 600);*/
}

void test_alarm_silence_high_pressure() {
  AlarmModule a;
#ifdef ARDUINO
  TEST_ASSERT_TRUE(a.silence(HIGH_PRESSURE, millis(), 60));
#else
  TEST_ASSERT_TRUE(a.silence(HIGH_PRESSURE, (uint32_t)timeSinceEpochMs(), 60));
#endif
  Timer t;
  while (t.elapsed() < 60) {
    TEST_ASSERT_FALSE(a.trigger(HIGH_PRESSURE, 431433, 600));
    // Make sure silencing high pressure doesn't silence low pressure
    TEST_ASSERT_TRUE(a.trigger(LOW_PRESSURE, 0, 0));
    a.run(t.elapsed());
    t.update();
  }
  // one additional run to ensure alarm gets to desired time
  a.run(t.elapsed());
  TEST_ASSERT_TRUE(a.trigger(HIGH_PRESSURE, 431433, 600));
}

void test_alarm_silence_low_pressure() {
  AlarmModule a;
#ifdef ARDUINO
  TEST_ASSERT_TRUE(a.silence(LOW_PRESSURE, millis(), 60));
#else
  TEST_ASSERT_TRUE(a.silence(LOW_PRESSURE, (uint32_t)timeSinceEpochMs(), 60));
#endif
  Timer t;
  while (t.elapsed() < 60) {
    TEST_ASSERT_FALSE(a.trigger(LOW_PRESSURE, 0, 0));
    // Make sure silencing low pressure doesn't silence high pressure
    TEST_ASSERT_TRUE(a.trigger(HIGH_PRESSURE, 431433, 600));
    a.run(t.elapsed());
    t.update();
  }
  // one additional run to ensure alarm gets to desired time
  a.run(t.elapsed());
  TEST_ASSERT_TRUE(a.trigger(LOW_PRESSURE, 0, 0));
}

void test_add_record_and_read_it() {
  AlarmController a;
  AlarmRecord ar(HIGH_PRESSURE, 124124, 600);
  bool res = a.addRecord(ar);
  TEST_ASSERT_TRUE(res);
  AlarmRecord ar2 = a.getRecord(0);
  TEST_ASSERT_EQUAL(ar2.alarmEvent, ar.alarmEvent);
  TEST_ASSERT_EQUAL(ar2.triggerTime, ar.triggerTime);
  TEST_ASSERT_EQUAL(ar2.triggerDuration, ar.triggerDuration);
}

// TODO: create more tests for all the different kinds of alarms

void process() {
  UNITY_BEGIN();
  // RUN_TEST(test_alarm_init);
  // RUN_TEST(test_alarm_reset);
  // RUN_TEST(test_alarm_set);
  // RUN_TEST(test_alarm_trigger);
  // RUN_TEST(test_alarm_silence_high_pressure);
  // RUN_TEST(test_alarm_silence_low_pressure);
  // RUN_TEST(test_add_record_and_read_it);
  UNITY_END();
}

#ifdef ARDUINO
#include <Arduino.h>
void setup() {
  // NOTE!!! Wait for > -->2 secs
  // if board doesn't support software reset via Serial.DTR/RTS
  delay(2000);
  process();
}
void loop() {
  //
}
#else
int main(int argc, char **argv) {
  process();
  return 0;
}
#endif
