#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <unity.h>

// #include <drive_simulator/drive_simulator.h>
#include <debug.h>
#include <drive.h>

using namespace VentOS;

Drive native_drive;
char client_data[64];

const int TARGET_VOL = 600;
int p;
// Test a single call to drive after doing a prep
void test_single_puff() {
  p = native_drive.pause_and_prep(1000, client_data, "Get ready!");
#ifdef ARDUINO
  delay(p);
#endif

  int d = native_drive.drive_flow(10, 200, 100, client_data,
                                  "I know you are really a PolyVent.");
  TEST_ASSERT_EQUAL(d, 10);
}

// Thest making a 600-ml puff in 1 second.
void test_big_puff() {
  p = native_drive.pause_and_prep(1000, client_data, "Get ready!");

#ifdef ARDUINO
  delay(p);
#endif

  const uint32_t MEASURED_PRESSURE = 200;
  const uint32_t TARGET_INSPIRE_TIME_MS = 1000;
  const uint32_t DRIVE_INTERVAL_MS = 10;
  const float TARGET_FLOW_MLPS =
      (float)TARGET_VOL / ((float)TARGET_INSPIRE_TIME_MS / 1000.0);
  const uint32_t INTERVALS = TARGET_INSPIRE_TIME_MS / DRIVE_INTERVAL_MS;
  for (uint32_t i = 0; i < INTERVALS; i++) {
    int d = native_drive.drive_flow(TARGET_FLOW_MLPS, MEASURED_PRESSURE,
                                    MEASURED_PRESSURE * 2, client_data,
                                    "I know you are really a PolyVent.");
    TEST_ASSERT_EQUAL(d, TARGET_FLOW_MLPS);
  }
}

void process() {
  UNITY_BEGIN();
  RUN_TEST(test_single_puff);
  RUN_TEST(test_big_puff);
  UNITY_END();
}

#ifdef ARDUINO
#include <Arduino.h>
void setup() {
  // NOTE!!! Wait for >2 secs
  // if board doesn't support software reset via Serial.DTR/RTS
  delay(2000);
  process();
}
void loop() {
  //
}
#else
int main(int argc, char **argv) {
  process();
  return 0;
}
#endif
